require 'faraday'
require 'nokogiri'
require "json"
$nc_cache = {}

module Wikipedia
  def Wikipedia.get(page_id); Wikipedia::page("https://en.wikipedia.org/wiki/#{page_id}"); end

  def Wikipedia.page(url);  WikipediaPage.new url; end
  
  def Wikipedia.search(query)
    url = "https://en.wikipedia.org/w/api.php?action=opensearch&search=#{query.gsub(' ', '+')}&format=json"
    d = JSON.parse Faraday.get(url).body
    Wikipedia::page d[3][0]
  end
end

class WikipediaPage
  attr_reader :body
  attr_reader :summary
  attr_reader :page
  attr_reader :infobox
  attr_reader :image
  
  def initialize url
    @body = Faraday.get(url).body
    doc = Nokogiri::HTML @body
    @summary = {text: [], links: []}
    @page = {}
    summarizing = true
    in_block = ""
    doc.css('*').each do |l|
      if summarizing
        if l.name == "p"
          l.css("a").each { |link|
            unless link["href"].include? "cite_note"
              @summary[:links] << {url: "https://en.wikipedia.org/"+link["href"], title: link.content}
            end
          }
          @summary[:text] << l.content.gsub(/\[\d*\]/, "")
        end

        if l.name == "h2"
          summarizing = false
          in_block = l.content.gsub "[edit]", ""
          @page[l.content.gsub "[edit]", ""] = {text: [], links: []}
        end
      else
        if l.name == "h2"
          in_block = l.content.gsub "[edit]", ""
          @page[in_block] = {text: [], links: []}
        elsif l.name == "p"
          l.css("a").each { |link|
            unless link["href"].include? "cite_note"
              @page[in_block][:links] << {url: "https://en.wikipedia.org"+link["href"], title: link.content}
            end
          }
          @page[in_block][:text] << l.content.gsub(/\[\d*\]/, "")
        end
      end
    end
    @infobox = {}
    doc.css('table').each do |tbl|
      if tbl.classes.include? "infobox" and tbl.classes.include? "hproduct"
        tbl.css("tr").each do |tr|
          ti = tr.css('th')[0]
          v = tr.css('td')[0]
          next if v == nil
          if ti != nil
            if v.css("a").length > 0
              @infobox[ti.content] = {url: "https://en.wikipedia.org/"+v.css("a")[0]["href"], text: v.css("a")[0].content}
            else
              @infobox[ti.content] = {url: "", text: v.content}
            end
          else
            @image = "https:"+v.css("img")[0]["src"]
          end
        end
      end
    end
  end

  def summarize(p = 1)
    out = ""
    i = 0
    begin
      while i < p
        out << @summary[:text][i]
        i += 1
      end
    rescue
      puts "Not enough summary blocks, ending early..."
    end

    return out
  end
end

module Startpage
  def Startpage.get(query, domain: "", description: "", title: "")
    query = query.gsub!(" ", "+")
    if $nc_cache.key? "sp.#{query}"; return $nc_cache["sp.#{query}"]; end
    res = Faraday.get "https://www.startpage.com/do/search?sc=GmwLxUlHaWET20&query=#{query}&cat=web"
    resp = []
    doc = Nokogiri::HTML res.body
    doc.css('div').each do |div|
      if div.classes.include? "w-gl__result"
        publish = true
        rtitle = div.css("h3").first.content.strip
        rdescription = div.css("p").first.content.strip
        rurl = div.css("a").first.content.strip
        if title != "" and not rtitle.downcase.include? title.downcase
          publish = false
        end
        if description != "" and not rdescription.downcase.include? description.downcase
          publish = false
        end
        if description != "" and not rurl.downcase.include? description.downcase
          publish = false
        end
        if publish
          resp << {title: rtitle, description: rdescription, url: rurl}
        end
      end
    end
    $nc_cache["sp.#{query}"] = resp
    resp    
  end
  
  def Startpage.images(query);
    query = query.gsub!(" ", "+")
    if $nc_cache.key? "spi.#{query}"; return $nc_cache["spi.#{query}"]; end
    
    res = Faraday.get "https://www.startpage.com/sp/search?abp=-1&language=english_uk&t=default&lui=english&query=#{query}&cat=pics&sc=k202N9qVugdS20"

    resp = []
    doc = Nokogiri::HTML res.body

    doc.css('div').each do |div|
      if div.classes.include? "image-container"
        publish = true
        meta = JSON.parse div["data-img-metadata"]
        resp << {image: meta["clickUrl"], description: meta["description"], url: meta["altClickUrl"], img_format: meta["format"]}
      end
    end
    $nc_cache["spi.#{query}"] = resp
    resp    
  end
end

module DDG
  def DDG.get(query, domain: "", description: "", title: "")
    query = query.gsub!(" ", "+")
    if $nc_cache.key? "ddg.#{query}"; return $nc_cache["ddg.#{query}"]; end
    resp = []
    res = Faraday.get "https://html.duckduckgo.com/html/?q=#{query}&norw=1&t=ffab&ia=web"
    doc = Nokogiri::HTML res.body
    c = {}
    publish = true
    doc.css("result__snippet result__a result__url", 'a').each do |link|
      if link.classes.include? "result__a"
        if title != "" and not link.content.strip.chomp.downcase.include? title.downcase
          publish = false
        end
        c[:title] = link.content.strip.chomp
      elsif link.classes.include? "result__url"
        if domain != "" and not link.content.strip.chomp.downcase.include? domain.downcase
          publish = false
        end
        c[:url] = "https://#{link.content.strip.chomp}"
      elsif link.classes.include? "result__snippet"
        if description != "" and not link.content.strip.chomp.downcase.include? description.downcase
          publish = false
        end
        c[:description] = link.content.strip.chomp
        resp.append c if publish == true
        c = {}
        publish = true
      end
    end
    $nc_cache["ddg.#{query}"] = resp
    resp
  end
end

module Qwant
  def Qwant.get(query, domain: "", description: "", title: "")
  end
end
