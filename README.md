# netcrawler.rb

Automated internet crawling library for Ruby.

```rb
require "netcrawler"
p = Wikipedia::search "deus ex game"
# => returns a Wikipedia Page object
=begin
Page contains 4 values:
  attr_reader :body # Raw HTML of the entire page
  [...]
  attr_reader :summary # Hash table of the summary
  attr_reader :page # Hash table of the page layout
  attr_reader :infobox # Hash table of the infobox sidebar
  attr_reader :image # URL to the pages cover art


=end

# And the following methods:
p.summarize
=begin
Deus Ex is a 2000 action role-playing game developed by Ion Storm and published by Eidos Interactive. Set in a cyberpunk-themed dystopian world in the year 2052, the game follows JC Denton, an agent of the fictional agency United Nations Anti-Terrorist Coalition (UNATCO), who is given superhuman abilities by nanotechnology, as he sets out to combat hostile forces in a world ravaged by inequality and a deadly plague. His missions entangle him in a conspiracy that brings him into conflict with the Triads, Majestic 12, and the Illuminati.
=end

# => returns a string of the pages summary. 
# => by default, only returns first paragraph of summary, but give it a


res = Startpage::get "deus ex"
# => or DDG::get
# => returns a hash table of results
# => supports keyword arguments for filters, :domain, :title, :description
# => if any keyword supplied, only shows results with a match, example;
res = Startpage::get "deus ex", domain: "steam"
# => only shows results from domains containing "steam", i.e. steampowered.com
# => so it'd likely result in getting the steam page

res = Startpage::images "deus ex"
# => returns a hash table from the engines image search (Only startpage currently)
```